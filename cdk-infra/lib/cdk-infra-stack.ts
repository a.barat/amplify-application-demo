import * as cdk from '@aws-cdk/core';
//import * as lambda from '@aws-cdk/aws-lambda';
//import * as apigw from '@aws-cdk/aws-apigateway';
import * as amplify from '@aws-cdk/aws-amplify';

export class CdkInfraStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here
    //// Lambda helloCDK
    //const helloCDK = new lambda.Function(this, "HelloCDKHandler", {
    //  runtime: lambda.Runtime.NODEJS_12_X,
    //  code: lambda.Code.fromAsset("lambda"),
    //  handler: "hellocdk.handler",
    //});
    //// ApiGateway Endpoint for helloCDK
    //new apigw.LambdaRestApi(this, "Endpoint", {
    //  handler: helloCDK,
    //});
    // Amplify app
    const amplifyApp = new amplify.App(this,"amplifyNextapp", {
      sourceCodeProvider: new amplify.GitLabSourceCodeProvider({
        owner: "a.barat",
        repository: "amplify-application-demo",
        oauthToken: cdk.SecretValue.secretsManager('ci-token', {
          jsonField: "secret",
        })
      })
    });

    amplifyApp.addBranch("develop");
    amplifyApp.addBranch("production");

  }
}
